package com.example.newassignment.cache

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PreferencesHelper @Inject constructor(context: Context) {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences(REF_PACKAGE_NAME, Context.MODE_PRIVATE)
    }

    var favoriteList: String?
        get() = sharedPreferences.getString(PREF_FAVORITE_CACHED, null)
        set(favoriteList) = sharedPreferences.edit().putString(PREF_FAVORITE_CACHED, favoriteList)
            .apply()

    companion object {
        private val REF_PACKAGE_NAME = BuildConfig.LIBRARY_PACKAGE_NAME
        private val PREF_FAVORITE_CACHED = "PREF_FAVORITE_CACHED"
    }
}