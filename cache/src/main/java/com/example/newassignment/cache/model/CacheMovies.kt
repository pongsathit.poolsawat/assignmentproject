package com.example.newassignment.cache.model

data class CacheMovie(

    val title: String?,

    val overview: String?,

    val release_date: String?,

    val poster_path: String?,

    val vote_average: Double?,

    val id: Int?

)