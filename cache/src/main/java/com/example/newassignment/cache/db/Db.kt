package com.example.newassignment.cache.db

object Db {
    object Table {

        const val TABLE_MOVIE = "movie"

        const val TITLE = "my_title"
        const val OVERVIEW = "my_overview"
        const val RELEASE_DATE = "my_release_date"
        const val POSTER_PATH = "my_poster_path"
        const val VOTE_AVERAGE = "my_vote_average"
        const val ID = "my_id"

        const val JSON_MOVIE_CACHED = "jsoncached"

        const val CREATE =
            "CREATE TABLE " + TABLE_MOVIE + " (" +
                    TITLE + " TEXT," +
                    OVERVIEW + " TEXT," +
                    RELEASE_DATE + " TEXT," +
                    POSTER_PATH + " TEXT," +
                    VOTE_AVERAGE + " DOUBLE," +
                    ID + " INTEGER PRIMARY KEY" +
                    "); "
    }
}