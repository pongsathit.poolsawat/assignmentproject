package com.example.newassignment.cache.db.constants

import com.example.newassignment.cache.db.Db

object Constants {

    internal val QUERY_GET_MOVIE = "SELECT * FROM " + Db.Table.TABLE_MOVIE

}