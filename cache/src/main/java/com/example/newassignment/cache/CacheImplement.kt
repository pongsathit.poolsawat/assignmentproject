package com.example.newassignment.cache

import android.database.sqlite.SQLiteDatabase
import com.example.newassignment.cache.db.Db
import com.example.newassignment.cache.db.DbOpenHelper
import com.example.newassignment.cache.db.constants.Constants
import com.example.newassignment.cache.db.mapper.CachedMovieMapper
import com.example.newassignment.cache.mapper.MovieEntityMapper
import com.example.newassignment.model.MovieEntity
import com.example.newassignment.repository.Cache
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Inject

class CacheImplement @Inject constructor(
    dbOpenHelper: DbOpenHelper,
    private val preferencesHelper: PreferencesHelper,
    private val movieEntityMapper: MovieEntityMapper,
    private val cachedMovieMapper: CachedMovieMapper
) : Cache {

    private var database: SQLiteDatabase = dbOpenHelper.writableDatabase

    internal fun getDatabase(): SQLiteDatabase {
        return database
    }

    override fun getResultMovie(query: String): Single<List<MovieEntity>> {
        return Single.defer {
            val updateCursor = database.rawQuery(Constants.QUERY_GET_MOVIE, null)
            val movies = mutableListOf<MovieEntity>()
            while (updateCursor.moveToNext()) {
                val cachedMovie = cachedMovieMapper.parseCursor(updateCursor)
                movies.add(movieEntityMapper.mapFromCached(cachedMovie))
            }
            updateCursor.close()
            Single.just<List<MovieEntity>>(movies)
        }
    }

    override fun getFavoriteMovie(): Single<List<MovieEntity>> {
        return Single.defer {
            val updatesCursor = database.rawQuery(Constants.QUERY_GET_MOVIE, null)
            val movies = mutableListOf<MovieEntity>()
            while (updatesCursor.moveToNext()) {
                val cacheMovie = cachedMovieMapper.parseCursor(updatesCursor)
                movies.add(movieEntityMapper.mapFromCached(cacheMovie))
            }
            updatesCursor.close()
            Single.just<List<MovieEntity>>(movies)
        }
    }

    override fun saveFavoriteMovie(movieEntity: MovieEntity): Completable {
        return Completable.defer {
            try {
                database.beginTransaction()
                val cached = movieEntityMapper.mapToCached(movieEntity)
                database.insert(
                    Db.Table.TABLE_MOVIE,
                    null,
                    cachedMovieMapper.toContentValues((cached))
                )
                database.setTransactionSuccessful()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                database.endTransaction()
            }
            Completable.complete()
        }
    }

    override fun deleteFavoriteMovie(id: Int): Completable {
        return Completable.defer {
            try {
                database.beginTransaction()
                database.delete(Db.Table.TABLE_MOVIE, Db.Table.ID + "=" + id, null)
                database.setTransactionSuccessful()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                database.endTransaction()
            }
            Completable.complete()
        }
    }

}