package com.example.newassignment.cache.db.mapper

import android.content.ContentValues
import android.database.Cursor
import com.example.newassignment.cache.db.Db
import com.example.newassignment.cache.model.CacheMovie
import javax.inject.Inject

class CachedMovieMapper @Inject constructor() :
    ModelDbMapper<CacheMovie> {
    override fun toContentValues(model: CacheMovie): ContentValues {
        val values = ContentValues()

        values.put(Db.Table.TITLE, model.title)
        values.put(Db.Table.OVERVIEW, model.overview)
        values.put(Db.Table.RELEASE_DATE, model.release_date)
        values.put(Db.Table.POSTER_PATH, model.poster_path)
        values.put(Db.Table.VOTE_AVERAGE, model.vote_average)
        values.put(Db.Table.ID, model.id)
        return values
    }

    override fun parseCursor(cursor: Cursor): CacheMovie {
        val title = cursor.getString(cursor.getColumnIndexOrThrow(Db.Table.TITLE))
        val overview = cursor.getString(cursor.getColumnIndexOrThrow(Db.Table.OVERVIEW))
        val release_date = cursor.getString(cursor.getColumnIndexOrThrow(Db.Table.RELEASE_DATE))
        val poster_path = cursor.getString(cursor.getColumnIndexOrThrow(Db.Table.POSTER_PATH))
        val vote_average = cursor.getDouble(cursor.getColumnIndexOrThrow(Db.Table.VOTE_AVERAGE))
        val id = cursor.getInt(cursor.getColumnIndexOrThrow(Db.Table.ID))

        return CacheMovie(
            title,
            overview,
            release_date,
            poster_path,
            vote_average,
            id
        )
    }
}