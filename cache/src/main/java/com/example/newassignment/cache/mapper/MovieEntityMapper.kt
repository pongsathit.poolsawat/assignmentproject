package com.example.newassignment.cache.mapper

import com.example.newassignment.cache.model.CacheMovie
import com.example.newassignment.model.MovieEntity
import javax.inject.Inject

class MovieEntityMapper @Inject constructor() :
    EntityMapper<CacheMovie, MovieEntity> {
    override fun mapFromCached(type: CacheMovie): MovieEntity {
        return MovieEntity(
            type.title,
            type.overview,
            type.release_date,
            type.poster_path,
            type.vote_average,
            type.id
        )
    }

    override fun mapToCached(type: MovieEntity): CacheMovie {
        return CacheMovie(
            type.title,
            type.overview,
            type.release_date,
            type.poster_path,
            type.vote_average,
            type.id
        )
    }


}