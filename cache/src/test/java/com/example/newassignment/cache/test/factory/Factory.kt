package com.example.newassignment.cache.test.factory

import com.example.newassignment.cache.test.factory.DataFactory.Factory.randomDouble
import com.example.newassignment.cache.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.cache.test.factory.DataFactory.Factory.randomUuid
import com.example.newassignment.model.MovieEntity

class Factory {

    companion object Factory {

        fun makeMovieEntity(): MovieEntity {
            return MovieEntity(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }
    }
}