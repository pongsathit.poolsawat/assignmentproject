package com.example.newassignment.cache

import com.example.newassignment.cache.db.Db
import com.example.newassignment.cache.db.DbOpenHelper
import com.example.newassignment.cache.db.mapper.CachedMovieMapper
import com.example.newassignment.cache.mapper.MovieEntityMapper
import com.example.newassignment.cache.test.factory.Factory.Factory.makeMovieEntity
import com.example.newassignment.model.MovieEntity
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(21))
class CacheImplementTest {

    private var preferencesHelper = PreferencesHelper(RuntimeEnvironment.application)
    private var movieEntityMapper = MovieEntityMapper()
    private var mapper = CachedMovieMapper()
    private var databaseHelper = CacheImplement(
        DbOpenHelper(RuntimeEnvironment.application),
        preferencesHelper,
        movieEntityMapper,
        mapper
    )

    @Before
    fun setUp() {
        databaseHelper.getDatabase().rawQuery("DELETE FROM " + Db.Table.TABLE_MOVIE, null)
    }

    @Test
    fun getMoviesComplete() {
        val movieEntity = makeMovieEntity()
        insertMovie(movieEntity)
        val testObserver = databaseHelper.getResultMovie("Batman").test()
        testObserver.assertComplete()
    }

    @Test
    fun getFavoriteComplete() {
        val movieEntity = makeMovieEntity()
        insertMovie(movieEntity)
        val testObserver = databaseHelper.getFavoriteMovie().test()
        testObserver.assertComplete()
    }

    @Test
    fun saveMovie() {
        val count = 1
        val movieEntity = makeMovieEntity()
        databaseHelper.saveFavoriteMovie(movieEntity).test()
        checkNumRow(count)
    }

    private fun insertMovie(cachedMovie: MovieEntity) {
        val cached = movieEntityMapper.mapToCached(cachedMovie)
        val database = databaseHelper.getDatabase()
        database.insertOrThrow(Db.Table.TABLE_MOVIE, null, mapper.toContentValues(cached))
    }

    private fun checkNumRow(expectedRows: Int) {
        val cursor = databaseHelper.getDatabase().query(
            Db.Table.TABLE_MOVIE,
            null, null, null, null, null, null
        )
        cursor.moveToFirst()
        val numberOfRows = cursor.count
        cursor.close()
        assertEquals(expectedRows, numberOfRows)
    }
}
