package com.example.newassignment.mapper

import com.example.newassignment.model.MovieEntity
import com.example.newassignment.model.MovieDetails
import javax.inject.Inject

open class MovieEntityMapper @Inject constructor() : EntityMapper<MovieDetails?, MovieEntity> {

    override fun mapFromRemote(type: MovieDetails?): MovieEntity {
        return MovieEntity(
            type?.title ?: "",
            type?.overview ?: "",
            type?.release_date ?: "",
            type?.poster_path ?: "",
            type?.vote_average ?: 0.0,
            type?.id ?: 0
        )
    }
}