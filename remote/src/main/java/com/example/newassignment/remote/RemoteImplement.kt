package com.example.newassignment.remote

import com.example.newassignment.model.MovieEntity
import com.example.newassignment.mapper.MovieEntityMapper
import com.example.newassignment.model.MovieDetails
import com.example.newassignment.repository.Remote
import io.reactivex.Single
import javax.inject.Inject

class RemoteImplement @Inject constructor(
    private val service: ApiService,
    private val movieEntityMapper: MovieEntityMapper
) : Remote {
    override fun getResultMovie(query: String): Single<List<MovieEntity>> {
        return service.getResultMovie(query).map {
            it.result?.map { movieDetails: MovieDetails ->
                movieEntityMapper.mapFromRemote(movieDetails)
            }
        }
    }

}
