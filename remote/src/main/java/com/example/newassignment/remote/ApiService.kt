package com.example.newassignment.remote

import com.example.newassignment.model.MovieDetails
import com.example.newassignment.model.MovieModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers("API-Key: 95ce69dc72383f00bc6ec694a1075d079e3dd210")
    @GET("/api/movies/search")
    fun getResultMovie(@Query("query") query: String): Single<MovieModel?>

}