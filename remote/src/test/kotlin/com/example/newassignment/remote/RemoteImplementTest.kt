package com.example.newassignment.remote

import com.example.newassignment.mapper.MovieEntityMapper
import com.example.newassignment.model.MovieModel
import com.example.newassignment.remote.test.factory.Factory.Factory.makeListMovieModel
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class RemoteImplementTest {

    private lateinit var remoteImplement: RemoteImplement

    @Mock
    private lateinit var service: ApiService

    @Mock
    private lateinit var movieEntityMapper: MovieEntityMapper

    @Before
    fun setup() {
        remoteImplement = RemoteImplement(
            service,
            movieEntityMapper
        )
    }

    @Test
    fun getResultMovie() {
        val movies = makeListMovieModel(20)
        stubServiceGetMovies(Single.just(movies))
        val testObserver = remoteImplement.getResultMovie("Batman").test()
        testObserver.assertComplete()
    }

    @Test
    fun getResultMovieSendEmpty() {
        val movies = makeListMovieModel(20)
        stubServiceGetMoviesEmpty(Single.just(movies))
        val testObserver = remoteImplement.getResultMovie("").test()
        testObserver.assertComplete()
    }

    private fun stubServiceGetMovies(single: Single<MovieModel?>) {
        whenever(service.getResultMovie("Batman")).thenReturn(single)
    }

    private fun stubServiceGetMoviesEmpty(single: Single<MovieModel?>) {
        whenever(service.getResultMovie("")).thenReturn(single)
    }
}