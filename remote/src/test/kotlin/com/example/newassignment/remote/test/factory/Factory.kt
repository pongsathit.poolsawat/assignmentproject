package com.example.newassignment.remote.test.factory

import com.example.newassignment.model.MovieDetails
import com.example.newassignment.model.MovieModel
import com.example.newassignment.remote.test.factory.DataFactory.Factory.randomDouble
import com.example.newassignment.remote.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.remote.test.factory.DataFactory.Factory.randomUuid

class Factory {

    companion object Factory {

        fun makeMovieModel(): MovieDetails {
            return MovieDetails(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }

        fun makeListMovieModel(count: Int): MovieModel? {
            val list = mutableListOf<MovieDetails>()
            repeat(count) {
                list.add(makeMovieModel())
            }
            return MovieModel(list)
        }
    }
}