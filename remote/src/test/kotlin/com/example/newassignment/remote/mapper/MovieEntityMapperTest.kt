package com.example.newassignment.remote.mapper

import com.example.newassignment.mapper.MovieEntityMapper
import com.example.newassignment.remote.test.factory.Factory
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MovieEntityMapperTest {

    private lateinit var entityMapper: MovieEntityMapper

    @Before
    fun setUp() {
        entityMapper = MovieEntityMapper()
    }

    @Test
    fun mapFromRemoteMapsData() {
        //Give
        val model = Factory.makeMovieModel()

        //When
        val entity = entityMapper.mapFromRemote(model)

        //Then
        assertEquals(model.id, entity.id)
        assertEquals(model.overview, entity.overview)
        assertEquals(model.poster_path, entity.poster_path)
        assertEquals(model.release_date, entity.release_date)
        assertEquals(model.title, entity.title)
        assertEquals(model.vote_average, entity.vote_average)
    }
}