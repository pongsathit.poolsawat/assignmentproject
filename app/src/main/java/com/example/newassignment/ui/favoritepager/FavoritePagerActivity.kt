package com.example.newassignment.ui.favoritepager

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newassignment.R
import com.example.newassignment.ui.favoritepager.adapter.FavoriteAdapter
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.model.MovieDisplay
import com.example.newassignment.ui.bus.Reload
import com.example.newassignment.ui.detailpager.DetailPagerActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_favorite.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

class FavoritePagerActivity : AppCompatActivity(), FavoritePagerContract.View {

    @Inject
    lateinit var favoritePresenter: FavoritePagerContract.Presenter

    var favoriteAdapter = FavoriteAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        tb_favorite.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(tb_favorite)
        AndroidInjection.inject(this)
        favoritePresenter.start()
        setRecyclerView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        favoritePresenter.stop()
    }

    override fun onRestart() {
        super.onRestart()
        this.recreate()
    }

    private fun setRecyclerView() {
        favoriteAdapter.apply {

            onItemClickListener = { movie ->
                this@FavoritePagerActivity?.let {
                    startActivity(
                        DetailPagerActivity.getIntent(
                            it,
                            MovieDisplay(
                                movie.title,
                                movie.vote_average,
                                movie.overview,
                                movie.poster_path,
                                movie.release_date,
                                movie.id
                            )
                        )
                    )
                }
            }
        }

        rv_favorite.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = favoriteAdapter
        }
    }

    override fun updateList(movies: List<Movie>) {
        favoriteAdapter.submitList(movies)
        favoriteAdapter.notifyDataSetChanged()
    }


    override fun setPresenter(presenter: FavoritePagerContract.Presenter) {
        favoritePresenter = presenter
    }

    @Subscribe
    fun subscribeReload(obj: Reload) {
        favoritePresenter.getFavorite()
    }
}