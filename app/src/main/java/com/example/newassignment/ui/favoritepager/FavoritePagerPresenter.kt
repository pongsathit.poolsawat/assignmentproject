package com.example.newassignment.ui.favoritepager

import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.model.Movie
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class FavoritePagerPresenter @Inject constructor(
    private val view: FavoritePagerContract.View,
    private val getCachedFavoriteUseCase: SingleUseCase<List<Movie>, Void?>,
    private val deleteCachedFavoriteUseCase: SingleUseCase<Boolean, Int>
) : FavoritePagerContract.Presenter {

    var movies: List<Movie>? = null

    override fun getFavorite() {
        getCachedFavoriteUseCase.execute(object : DisposableSingleObserver<List<Movie>>() {
            override fun onSuccess(movies: List<Movie>) {
                updateMovieList(movies)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })


    }

    private fun updateMovieList(movieList: List<Movie>) {
        this.movies = movieList
        setData()
    }

    override fun setData() {
        movies?.let {
            view.updateList(it)
        }
    }

    override fun start() {
        getFavorite()
    }

    override fun stop() {
        getCachedFavoriteUseCase.dispose()
        deleteCachedFavoriteUseCase.dispose()
    }

}