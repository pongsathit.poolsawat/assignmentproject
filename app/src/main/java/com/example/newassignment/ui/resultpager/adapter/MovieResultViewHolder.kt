package com.example.newassignment.ui.resultpager.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.newassignment.R
import com.example.newassignment.domain.model.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var posterUrl = ""

    fun bindData(
        item: Movie,
        listener: ((item: Movie) -> Unit)?
    ) = with(itemView) {
        tv_title?.text = item.title
        tv_overview?.text = item.overview
        tv_date?.text = item.release_date
        posterUrl = "https://image.tmdb.org/t/p/w92${item.poster_path}"

        val requestOptions = RequestOptions
            .errorOf(R.drawable.ic_error_red_24dp)
        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(posterUrl)
            .into(iv_poster)

        itemView.setOnClickListener {
            listener?.invoke(item)
        }
    }
}