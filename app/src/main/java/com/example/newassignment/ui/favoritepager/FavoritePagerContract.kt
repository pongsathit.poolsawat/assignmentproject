package com.example.newassignment.ui.favoritepager

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.ui.base.BasePresenter
import com.example.newassignment.ui.base.BaseView

interface FavoritePagerContract {

    interface View : BaseView<Presenter> {
        fun updateList(movies: List<Movie>)
    }

    interface Presenter : BasePresenter {
        fun getFavorite()
        fun setData()
    }
}