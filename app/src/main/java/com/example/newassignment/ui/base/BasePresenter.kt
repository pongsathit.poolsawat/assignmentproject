package com.example.newassignment.ui.base

interface BasePresenter {

    fun start()

    fun stop()

}