package com.example.newassignment.ui.detailpager

import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.model.Movie
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class DetailPagerPresenter @Inject constructor(
    private val view: DetailPagerContract.View,
    private val getCachedFavoriteUseCase: SingleUseCase<List<Movie>, Void?>,
    private val saveCachedFavoriteUseCase: SingleUseCase<Boolean, Movie>,
    private val deleteCachedFavoriteUseCase: SingleUseCase<Boolean, Int>
) : DetailPagerContract.Presenter {

    var movies: List<Movie>? = null

    override fun onFavoriteClick(movie: Movie) {
        saveCachedFavoriteUseCase.execute(object : DisposableSingleObserver<Boolean>() {
            override fun onSuccess(t: Boolean) {
                getFavoriteList()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }

        }, movie)
    }

    private fun getFavoriteList() {
        getCachedFavoriteUseCase.execute(object : DisposableSingleObserver<List<Movie>>() {
            override fun onSuccess(cachedMovies: List<Movie>) {
                movies = cachedMovies
                val favoriteListId = mutableListOf<Int>()
                cachedMovies.forEach {
                    it.id?.let { id ->
                        favoriteListId.add(id)
                    }
                }
                updateFavoriteListId(favoriteListId)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }

        })
    }

    private fun updateFavoriteListId(listFavorite: List<Int>) {
        view.updateFavoriteList(listFavorite)
        this.movies?.let {
            updateMovieList(it)
        }
    }

    override fun setFavoriteButton(movie: Movie, listFavorite: List<Int>) {
        if (listFavorite.contains(movie.id)) {
            view.setFavoriteButton("Unfavorite")
        } else if (!listFavorite.contains(movie.id)) {
            view.setFavoriteButton("Favorite")
        }
    }

    override fun setActionFavorite(status: String, movie: Movie): String {
        return if (status == "Favorite") {
            onFavoriteClick(movie)
            "Unfavorite"
        } else {
            onUnFavoriteClick(movie.id)
            "Favorite"
        }
    }

    private fun updateMovieList(movieList: List<Movie>) {
        this.movies = movieList
    }

    override fun onUnFavoriteClick(id: Int?) {
        deleteCachedFavoriteUseCase.execute(object : DisposableSingleObserver<Boolean>() {
            override fun onSuccess(t: Boolean) {
                getFavoriteList()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }

        }, id)
    }

    override fun start() {
        getFavoriteList()
    }

    override fun stop() {
        getCachedFavoriteUseCase.dispose()
        saveCachedFavoriteUseCase.dispose()
        deleteCachedFavoriteUseCase.dispose()
    }
}