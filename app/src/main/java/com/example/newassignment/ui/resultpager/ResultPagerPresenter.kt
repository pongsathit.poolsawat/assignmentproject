package com.example.newassignment.ui.resultpager

import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.model.Movie
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ResultPagerPresenter @Inject constructor(
    private val view: ResultPagerContract.View,
    private val getResultMovieUseCase: SingleUseCase<List<Movie>, String>
) : ResultPagerContract.Presenter {

    var movies: List<Movie>? = null

    init {
        view.setPresenter(this)
    }

    override fun getResultMovie(query: String?) {
        getResultMovieUseCase.execute(GetMoviesSubscriber(), query)
    }

    override fun start() {
        getResultMovie(view.getSearch())
    }

    override fun stop() {
        getResultMovieUseCase.dispose()
    }

    private fun setData() {
        movies?.let { movies ->
            view.updateList(movies)
        }
    }

    private fun updateMovieList(movieList: List<Movie>) {
        this.movies = movieList
        setData()
    }

    inner class GetMoviesSubscriber : DisposableSingleObserver<List<Movie>>() {
        override fun onSuccess(movies: List<Movie>) {
            updateMovieList(movies)
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

    }

}