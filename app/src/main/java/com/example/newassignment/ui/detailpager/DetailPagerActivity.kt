package com.example.newassignment.ui.detailpager

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.newassignment.R
import com.example.newassignment.SearchPageActivity
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.model.MovieDisplay
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_display.*
import javax.inject.Inject

class DetailPagerActivity : AppCompatActivity(), DetailPagerContract.View {

    @Inject
    lateinit var detailPresenter: DetailPagerContract.Presenter

    companion object {
        private const val EXTRA_DISPLAY = "DISPLAY"

        fun getIntent(context: Context, movieDisplay: MovieDisplay): Intent {
            return Intent(context, DetailPagerActivity::class.java)
                .putExtra(EXTRA_DISPLAY, movieDisplay)

        }
    }

    var listFavorite = mutableListOf<Int>()
    var stat: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)
        toolbardisplay.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbardisplay)
        AndroidInjection.inject(this)
        detailPresenter.start()

        val getList = Movie(
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.title,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.overview,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.date,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.posterUrl,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.vote,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.id
        )
        setView(getList)
        addActionFavoriteButton(getList)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_display, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        } else if (id == R.id.tv_back) {
            val intent = Intent(applicationContext, SearchPageActivity::class.java)
            startActivity(intent)
        }
        return true
    }

    private fun setView(movie: Movie) {
        tv_description.movementMethod = ScrollingMovementMethod()
        tv_name.text = movie.title
        tv_vote.text = ("Average votes: ${movie.vote_average}")
        tv_description.text = movie.overview
        val poster = "https://image.tmdb.org/t/p/w92${movie.poster_path}"

        val requestOptions = RequestOptions
            .errorOf(R.drawable.ic_sentiment_very_dissatisfied_black_24dp)
        Glide.with(applicationContext)
            .setDefaultRequestOptions(requestOptions)
            .load(poster)
            .into(iv_movie)
    }

    override fun updateFavoriteList(listfavorite: List<Int>) {
        listFavorite = listfavorite.toMutableList()

        val getList = Movie(
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.title,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.overview,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.date,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.posterUrl,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.vote,
            intent.getParcelableExtra<MovieDisplay>(EXTRA_DISPLAY)?.id
        )

        detailPresenter.setFavoriteButton(getList, listFavorite)
    }

    override fun setPresenter(presenter: DetailPagerContract.Presenter) {
        detailPresenter = presenter
    }

    override fun setFavoriteButton(status: String) {
        bt_favorite.text = status
        stat = bt_favorite.text.toString()
    }

    @SuppressLint("SetTextI18n")
    override fun addActionFavoriteButton(movie: Movie) {
        bt_favorite.setOnClickListener {
            bt_favorite.text = detailPresenter.setActionFavorite(stat, movie)
        }

    }

}