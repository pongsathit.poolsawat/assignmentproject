package com.example.newassignment.ui.resultpager

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.ui.base.BasePresenter
import com.example.newassignment.ui.base.BaseView

interface ResultPagerContract {

    interface View : BaseView<Presenter> {
        fun updateList(movies: List<Movie>)
        fun getSearch(): String?
    }

    interface Presenter : BasePresenter {
        fun getResultMovie(query: String?)
    }
}