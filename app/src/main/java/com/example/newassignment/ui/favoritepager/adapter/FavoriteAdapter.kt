package com.example.newassignment.ui.favoritepager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.newassignment.R
import com.example.newassignment.domain.model.Movie

class FavoriteAdapter : ListAdapter<Movie, RecyclerView.ViewHolder>(BaseFileUploadDiffCallback()) {

    var onItemClickListener: ((item: Movie) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.item_favorite,
                parent, false
            )
        return FavoriteViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FavoriteViewHolder) {
            val data = getItem(position) as Movie
            holder.bindData(data, onItemClickListener)
        }
    }

}

class BaseFileUploadDiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

}