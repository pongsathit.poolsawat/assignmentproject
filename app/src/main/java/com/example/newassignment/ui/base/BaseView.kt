package com.example.newassignment.ui.base

interface BaseView<in T : BasePresenter> {

    fun setPresenter(presenter: T)

}