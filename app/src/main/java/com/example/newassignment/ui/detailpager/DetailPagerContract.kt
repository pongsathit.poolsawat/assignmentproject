package com.example.newassignment.ui.detailpager

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.ui.base.BasePresenter
import com.example.newassignment.ui.base.BaseView

interface DetailPagerContract {

    interface View : BaseView<Presenter> {
        fun addActionFavoriteButton(movie: Movie)
        fun updateFavoriteList(listfavorite: List<Int>)
        fun setFavoriteButton(status: String)
    }

    interface Presenter : BasePresenter {
        fun onFavoriteClick(movie: Movie)
        fun onUnFavoriteClick(id: Int?)
        fun setFavoriteButton(movie: Movie, listFavorite: List<Int>)
        fun setActionFavorite(status: String, movie: Movie): String
    }
}