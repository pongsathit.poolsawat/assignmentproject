package com.example.newassignment.ui.favoritepager.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.newassignment.R
import com.example.newassignment.domain.model.Movie
import kotlinx.android.synthetic.main.item_favorite.view.*

class FavoriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(
        item: Movie,
        listener: ((item: Movie) -> Unit)?
    ) = with(itemView) {

        tv_titlefavorite?.text = item.title
        tv_datefavorite?.text = item.release_date
        tv_overviewfavorite?.text = item.overview

        val posterUrl = "https://image.tmdb.org/t/p/w92${item.poster_path}"

        val requestOptions = RequestOptions
            .errorOf(R.drawable.ic_error_red_24dp)
        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(posterUrl)
            .into(iv_posterfavorite)

        itemView.setOnClickListener { listener?.invoke(item) }
    }
}