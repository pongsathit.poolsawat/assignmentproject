package com.example.newassignment.ui.resultpager

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newassignment.R
import com.example.newassignment.SearchPageActivity.Companion.EXTRA_SEARCH
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.model.MovieDisplay
import com.example.newassignment.ui.bus.ReloadMobile
import com.example.newassignment.ui.detailpager.DetailPagerActivity
import com.example.newassignment.ui.resultpager.adapter.MovieRecyclerAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_result.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

class ResultPagerActivity : AppCompatActivity(), ResultPagerContract.View {

    @Inject
    lateinit var resultPagerPresenter: ResultPagerContract.Presenter

    var movieAdapter = MovieRecyclerAdapter()

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        tb_title.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(tb_title)
        AndroidInjection.inject(this)
        resultPagerPresenter.start()
        setupRecyclerView()

    }

    override fun onDestroy() {
        super.onDestroy()
        resultPagerPresenter.stop()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        }
        return true
    }


    private fun setupRecyclerView() {
        movieAdapter.apply {
            onItemClickListener = { movie ->
                this@ResultPagerActivity?.let {
                    startActivity(
                        DetailPagerActivity.getIntent(
                            it,
                            MovieDisplay(
                                movie.title,
                                movie.vote_average,
                                movie.overview,
                                movie.poster_path,
                                movie.release_date,
                                movie.id
                            )
                        )
                    )

                }
            }

            rv_movie.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = movieAdapter
            }
        }
    }

    override fun updateList(movies: List<Movie>) {
        movieAdapter.submitList(movies)
        movieAdapter.notifyDataSetChanged()
    }

    override fun getSearch(): String? {
        return intent.getStringExtra(EXTRA_SEARCH)
    }

    override fun setPresenter(presenter: ResultPagerContract.Presenter) {
        resultPagerPresenter = presenter
    }

    @Subscribe
    fun subScribeReload(obj: ReloadMobile) {
        resultPagerPresenter.getResultMovie(obj.query)
    }


}