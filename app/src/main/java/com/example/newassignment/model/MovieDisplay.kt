package com.example.newassignment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class MovieDisplay(
    val title: String?,
    val vote: Double?,
    val overview: String?,
    val posterUrl: String?,
    val date: String?,
    val id: Int?
) : Parcelable