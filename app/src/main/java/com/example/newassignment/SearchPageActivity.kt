package com.example.newassignment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newassignment.adapter.SearchClickListener
import com.example.newassignment.adapter.SearchHistoryAdapter
import com.example.newassignment.ui.favoritepager.FavoritePagerActivity
import com.example.newassignment.ui.resultpager.ResultPagerActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.sv_movie as searchViewMovie
import kotlinx.android.synthetic.main.activity_main.toolbartitle as toolbarTitle

class SearchPageActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_SEARCH = "com.example.newassignment"
    }

    private lateinit var searchAdapter: SearchHistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbarTitle)
        setSearchView()
        setSearchAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.tv_favorite) {
            val intent = Intent(applicationContext, FavoritePagerActivity::class.java)
            startActivity(intent)
        }
        return true
    }

    private fun setSearchView() {
        val searchArrayList = ArrayList<String>()

        searchViewMovie.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                searchArrayList.add(query.toString())
                searchViewMovie.setQuery("", true)
                searchAdapter.addSearchList(searchArrayList)

                val intent = Intent(applicationContext, ResultPagerActivity::class.java).apply {
                    putExtra(EXTRA_SEARCH, query)
                }
                startActivity(intent)

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })
    }

    private fun setSearchAdapter() {
        val listener = object : SearchClickListener {
            override fun onItemClick(search: String) {
                val intent = Intent(applicationContext, ResultPagerActivity::class.java).apply {
                    putExtra(EXTRA_SEARCH, search)
                }
                startActivity(intent)
            }
        }

        searchAdapter = SearchHistoryAdapter(listener)

        rv_search.adapter = searchAdapter
        rv_search.layoutManager = LinearLayoutManager(applicationContext)

    }


}
