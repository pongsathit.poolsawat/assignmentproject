package com.example.newassignment.injection.module

import com.example.newassignment.domain.interactor.getResultMovie.GetResultMovie
import com.example.newassignment.injection.scopes.PerActivity
import com.example.newassignment.ui.resultpager.ResultPagerActivity
import com.example.newassignment.ui.resultpager.ResultPagerContract
import com.example.newassignment.ui.resultpager.ResultPagerPresenter
import dagger.Module
import dagger.Provides

@Module
open class ResultPagerModule {

    @Provides
    @PerActivity
    internal fun provideMainView(resultPagerActivity: ResultPagerActivity): ResultPagerContract.View {
        return resultPagerActivity
    }

    @Provides
    @PerActivity
    internal fun provideMainPresenter(
        mainView: ResultPagerContract.View,
        getResultMovie: GetResultMovie
    ): ResultPagerContract.Presenter {
        return ResultPagerPresenter(
            mainView,
            getResultMovie
        )
    }
}