package com.example.newassignment.injection.component

import android.app.Application
import com.example.newassignment.injection.module.ApplicationModule
import com.example.newassignment.injection.module.BindingModule
import com.example.newassignment.injection.scopes.PerApplication
import com.example.newassignment.ui.MainApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule


@PerApplication
@Component(
    modules = [
        ApplicationModule::class,
        AndroidSupportInjectionModule::class,
        BindingModule::class
    ]
)

interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: MainApplication)
}

