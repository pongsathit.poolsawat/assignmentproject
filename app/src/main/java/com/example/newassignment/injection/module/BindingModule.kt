package com.example.newassignment.injection.module

import com.example.newassignment.injection.scopes.PerActivity
import com.example.newassignment.ui.detailpager.DetailPagerActivity
import com.example.newassignment.ui.favoritepager.FavoritePagerActivity
import com.example.newassignment.ui.resultpager.ResultPagerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [ResultPagerModule::class])
    abstract fun bindResultPagerActivity(): ResultPagerActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DetailPagerModule::class])
    abstract fun bindDetailPagerActivity(): DetailPagerActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [FavoritePagerModule::class])
    abstract fun bindFavoritePagerActivity(): FavoritePagerActivity

}