package com.example.newassignment.injection.module

import android.app.Application
import android.content.Context
import com.example.newassignment.BuildConfig
import com.example.newassignment.DataRepository
import com.example.newassignment.cache.CacheImplement
import com.example.newassignment.cache.PreferencesHelper
import com.example.newassignment.cache.db.DbOpenHelper
import com.example.newassignment.cache.db.mapper.CachedMovieMapper
import com.example.newassignment.domain.repository.Repository
import com.example.newassignment.injection.scopes.PerApplication
import com.example.newassignment.cache.mapper.MovieEntityMapper
import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.executor.JobExecutor
import com.example.newassignment.mapper.MovieMapper
import com.example.newassignment.remote.ApiService
import com.example.newassignment.remote.ApiServiceFactory
import com.example.newassignment.remote.RemoteImplement
import com.example.newassignment.repository.Cache
import com.example.newassignment.repository.Remote
import com.example.newassignment.source.DataStoreFactory
import com.example.newassignment.ui.UiThread
import dagger.Module
import dagger.Provides

@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun provideRepository(
        factory: DataStoreFactory,
        movieMapper: MovieMapper
    ): Repository {
        return DataRepository(factory, movieMapper)
    }

    @Provides
    @PerApplication
    internal fun provideCache(
        factory: DbOpenHelper,
        helper: PreferencesHelper,
        movieEntityMapper: MovieEntityMapper,
        cachedMovieMapper: CachedMovieMapper
    ): Cache {
        return CacheImplement(
            factory,
            helper,
            movieEntityMapper,
            cachedMovieMapper
        )
    }

    @Provides
    @PerApplication
    internal fun provideRemote(
        service: ApiService,
        movieEntityMapper: com.example.newassignment.mapper.MovieEntityMapper
    ): Remote {
        return RemoteImplement(
            service,
            movieEntityMapper
        )
    }

    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    internal fun provideService(): ApiService {
        return ApiServiceFactory.makeService(BuildConfig.DEBUG)
    }

}