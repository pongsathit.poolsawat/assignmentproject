package com.example.newassignment.injection.module

import com.example.newassignment.domain.interactor.deleteFavoriteMovie.DeleteFavoriteMovie
import com.example.newassignment.domain.interactor.getFavoriteList.GetFavoriteList
import com.example.newassignment.injection.scopes.PerActivity
import com.example.newassignment.ui.favoritepager.FavoritePagerActivity
import com.example.newassignment.ui.favoritepager.FavoritePagerContract
import com.example.newassignment.ui.favoritepager.FavoritePagerPresenter
import dagger.Module
import dagger.Provides


@Module
open class FavoritePagerModule {

    @Provides
    @PerActivity
    internal fun provideFavoriteView(favoritePagerActivity: FavoritePagerActivity): FavoritePagerContract.View {
        return favoritePagerActivity
    }

    @Provides
    @PerActivity
    internal fun provideFavoritePresenter(
        mainView: FavoritePagerContract.View,
        getFavoriteList: GetFavoriteList,
        deleteFavoriteMovie: DeleteFavoriteMovie
    ): FavoritePagerContract.Presenter {
        return FavoritePagerPresenter(
            mainView,
            getFavoriteList,
            deleteFavoriteMovie
        )
    }

}