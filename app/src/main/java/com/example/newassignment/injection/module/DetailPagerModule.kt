package com.example.newassignment.injection.module

import com.example.newassignment.domain.interactor.deleteFavoriteMovie.DeleteFavoriteMovie
import com.example.newassignment.domain.interactor.getFavoriteList.GetFavoriteList
import com.example.newassignment.domain.interactor.saveFavoriteMovie.SaveFavoriteMovie
import com.example.newassignment.injection.scopes.PerActivity
import com.example.newassignment.ui.detailpager.DetailPagerActivity
import com.example.newassignment.ui.detailpager.DetailPagerContract
import com.example.newassignment.ui.detailpager.DetailPagerPresenter
import dagger.Module
import dagger.Provides

@Module
open class DetailPagerModule {

    @Provides
    @PerActivity
    internal fun provideMainView(detailPagerActivity: DetailPagerActivity): DetailPagerContract.View {
        return detailPagerActivity
    }

    @Provides
    @PerActivity
    internal fun provideMainPresenter(
        mainView: DetailPagerContract.View,
        getCachedFavorite: GetFavoriteList,
        saveCachedFavorite: SaveFavoriteMovie,
        deleteCachedFavorite: DeleteFavoriteMovie
    ): DetailPagerContract.Presenter {
        return DetailPagerPresenter(
            mainView,
            getCachedFavorite,
            saveCachedFavorite,
            deleteCachedFavorite
        )
    }
}