package com.example.newassignment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newassignment.R

class SearchHistoryViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
) {
    private val searchHistory: TextView = itemView.findViewById(R.id.tv_search)

    fun bind(search: String, listener: SearchClickListener) {

        searchHistory.text = search

        itemView.setOnClickListener {
            listener.onItemClick(
                searchHistory.text.toString()
            )
        }
    }


}