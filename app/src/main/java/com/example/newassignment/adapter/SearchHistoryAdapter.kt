package com.example.newassignment.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class SearchHistoryAdapter(private val listener: SearchClickListener) :
    RecyclerView.Adapter<SearchHistoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHistoryViewHolder {
        return SearchHistoryViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return searchList.count()
    }

    override fun onBindViewHolder(holder: SearchHistoryViewHolder, position: Int) {
        holder.bind(searchList[position], listener)
    }

    private var searchList: ArrayList<String> = arrayListOf()

    fun addSearchList(searchArrayList: ArrayList<String>) {
        searchList = searchArrayList
        searchList.reverse()
        notifyDataSetChanged()
    }
}


interface SearchClickListener {
    fun onItemClick(
        search: String
    )
}