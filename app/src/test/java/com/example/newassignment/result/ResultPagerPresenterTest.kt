package com.example.newassignment.result

import com.example.newassignment.domain.interactor.getResultMovie.GetResultMovie
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.result.MovieFactory.Factory.makeListMovie
import com.example.newassignment.ui.resultpager.ResultPagerContract
import com.example.newassignment.ui.resultpager.ResultPagerPresenter
import com.nhaarman.mockitokotlin2.*
import io.reactivex.observers.DisposableSingleObserver
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ResultPagerPresenterTest {

    lateinit var presenter: ResultPagerPresenter
    lateinit var view: ResultPagerContract.View
    lateinit var getResultMovieUseCase: GetResultMovie

    lateinit var captorGetResultMovieUseCase: KArgumentCaptor<DisposableSingleObserver<List<Movie>>>

    @Before
    fun setUp() {
        view = mock()
        getResultMovieUseCase = mock()

        captorGetResultMovieUseCase = argumentCaptor()

        presenter = ResultPagerPresenter(
            view,
            getResultMovieUseCase
        )
    }

    @Test
    fun getResultMovieListSuccess() {
        //Give
        val listMovie = makeListMovie(20)

        //When
        presenter.getResultMovie("")

        //Then
        verify(getResultMovieUseCase).execute(captorGetResultMovieUseCase.capture(), eq(""))
        captorGetResultMovieUseCase.firstValue.onSuccess(listMovie)
        assertEquals(presenter.movies, listMovie)
    }

    @Test
    fun getResultMovieListError() {
        //When
        presenter.getResultMovie("")

        //Then
        verify(getResultMovieUseCase).execute(captorGetResultMovieUseCase.capture(), eq(""))
        captorGetResultMovieUseCase.firstValue.onError(Throwable())
    }
}