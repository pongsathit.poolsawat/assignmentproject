package com.example.newassignment.result

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.test.factory.DataFactory.Factory.randomDouble
import com.example.newassignment.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.test.factory.DataFactory.Factory.randomUuid

class MovieFactory {

    companion object Factory {
        fun makeMovie(): Movie {
            return Movie(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }

        fun makeListMovie(count: Int): List<Movie> {
            val list = mutableListOf<Movie>()
            repeat(count) {
                list.add(makeMovie())
            }
            return list
        }
    }
}