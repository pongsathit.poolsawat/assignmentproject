package com.example.newassignment.favorite

import com.example.newassignment.domain.interactor.deleteFavoriteMovie.DeleteFavoriteMovie
import com.example.newassignment.domain.interactor.getFavoriteList.GetFavoriteList
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.result.MovieFactory.Factory.makeListMovie
import com.example.newassignment.ui.favoritepager.FavoritePagerContract
import com.example.newassignment.ui.favoritepager.FavoritePagerPresenter
import com.nhaarman.mockitokotlin2.*
import io.reactivex.observers.DisposableSingleObserver
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class FavoritePagerPresenterTest {

    lateinit var presenter: FavoritePagerPresenter
    lateinit var view: FavoritePagerContract.View
    lateinit var getCachedFavoriteUseCase: GetFavoriteList
    lateinit var deleteCachedFavoriteUseCase: DeleteFavoriteMovie

    lateinit var captorGetFavoriteList: KArgumentCaptor<DisposableSingleObserver<List<Movie>>>
    lateinit var captorDeleteFavoriteMovie: KArgumentCaptor<DisposableSingleObserver<Boolean>>

    @Before
    fun setUp() {
        view = mock()
        getCachedFavoriteUseCase = mock()
        deleteCachedFavoriteUseCase = mock()

        captorGetFavoriteList = argumentCaptor()
        captorDeleteFavoriteMovie = argumentCaptor()

        presenter = FavoritePagerPresenter(
            view,
            getCachedFavoriteUseCase,
            deleteCachedFavoriteUseCase
        )
    }

    @Test
    fun getFavorite() {
        //Give
        val listMovie = makeListMovie(20)

        //When
        presenter.getFavorite()

        //Then
        verify(getCachedFavoriteUseCase).execute(captorGetFavoriteList.capture(), eq(null))
        captorGetFavoriteList.firstValue.onSuccess(listMovie)
        assertEquals(presenter.movies, listMovie)
        verify(view).updateList(any())
    }

}