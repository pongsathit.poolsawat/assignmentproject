package com.example.newassignment.detail

import com.example.newassignment.domain.interactor.deleteFavoriteMovie.DeleteFavoriteMovie
import com.example.newassignment.domain.interactor.getFavoriteList.GetFavoriteList
import com.example.newassignment.domain.interactor.saveFavoriteMovie.SaveFavoriteMovie
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.result.MovieFactory.Factory.makeListMovie
import com.example.newassignment.result.MovieFactory.Factory.makeMovie
import com.example.newassignment.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.ui.detailpager.DetailPagerContract
import com.example.newassignment.ui.detailpager.DetailPagerPresenter
import com.nhaarman.mockitokotlin2.*
import io.reactivex.observers.DisposableSingleObserver
import org.junit.Before
import org.junit.Test

class DetailPagerPresenterTest {
    lateinit var presenter: DetailPagerPresenter
    lateinit var view: DetailPagerContract.View
    lateinit var getCachedFavoriteUseCase: GetFavoriteList
    lateinit var saveCachedFavoriteUseCase: SaveFavoriteMovie
    lateinit var deleteCachedFavoriteUseCase: DeleteFavoriteMovie

    lateinit var captorGetFavoriteMovie: KArgumentCaptor<DisposableSingleObserver<List<Movie>>>
    lateinit var captorSaveFavoriteMovie: KArgumentCaptor<DisposableSingleObserver<Boolean>>
    lateinit var captorDeleteFavoriteMovie: KArgumentCaptor<DisposableSingleObserver<Boolean>>

    @Before
    fun setUp() {
        view = mock()
        getCachedFavoriteUseCase = mock()
        saveCachedFavoriteUseCase = mock()
        deleteCachedFavoriteUseCase = mock()

        captorDeleteFavoriteMovie = argumentCaptor()
        captorGetFavoriteMovie = argumentCaptor()
        captorSaveFavoriteMovie = argumentCaptor()

        presenter = DetailPagerPresenter(
            view,
            getCachedFavoriteUseCase,
            saveCachedFavoriteUseCase,
            deleteCachedFavoriteUseCase
        )
    }

    @Test
    fun onFavoriteClickSuccess() {
        //Give
        val movie = makeMovie()
        val listMovie = makeListMovie(20)

        //When
        presenter.onFavoriteClick(movie)

        //Then
        verify(saveCachedFavoriteUseCase).execute(captorSaveFavoriteMovie.capture(), eq(movie))
        captorSaveFavoriteMovie.firstValue.onSuccess(true)
        verify(getCachedFavoriteUseCase).execute(captorGetFavoriteMovie.capture(), eq(null))
        captorGetFavoriteMovie.firstValue.onSuccess(listMovie)
        verify(view).updateFavoriteList(any())
    }

    @Test
    fun onFavoriteClickError() {
        //Give
        val movie = makeMovie()

        //When
        presenter.onFavoriteClick(movie)

        //Then
        verify(saveCachedFavoriteUseCase).execute(captorSaveFavoriteMovie.capture(), eq(movie))
        captorSaveFavoriteMovie.firstValue.onError(Throwable())
    }

    @Test
    fun onUnfavoriteClickSuccess() {
        //Give
        val id = randomInt()
        val listMovie = makeListMovie(20)

        //When
        presenter.onUnFavoriteClick(id)

        //Then
        verify(deleteCachedFavoriteUseCase).execute(captorDeleteFavoriteMovie.capture(), eq(id))
        captorDeleteFavoriteMovie.firstValue.onSuccess(true)

        verify(getCachedFavoriteUseCase).execute(captorGetFavoriteMovie.capture(), eq(null))
        captorGetFavoriteMovie.firstValue.onSuccess(listMovie)
    }

    @Test
    fun onUnfavoriteClickError() {
        //Give
        val id = randomInt()

        //When
        presenter.onUnFavoriteClick(id)

        //Then
        verify(deleteCachedFavoriteUseCase).execute(captorDeleteFavoriteMovie.capture(), eq(id))
        captorDeleteFavoriteMovie.firstValue.onError(Throwable())
    }
}