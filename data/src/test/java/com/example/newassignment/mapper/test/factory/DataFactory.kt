package com.example.newassignment.mapper.test.factory

import java.util.concurrent.ThreadLocalRandom

class DataFactory {

    companion object Factory {

        fun randomUuid(): String {
            return java.util.UUID.randomUUID().toString()
        }

        fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt()
        }

        fun randomDouble(): Double {
            return ThreadLocalRandom.current().nextDouble()
        }
    }
}