package com.example.newassignment.mapper.source

import com.example.newassignment.mapper.test.factory.Factory.Factory.makeListMovieEntity
import com.example.newassignment.repository.Remote
import com.example.newassignment.source.RemoteDataStore
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class RemoteDataStoreTest {

    private lateinit var remoteDataStore: RemoteDataStore

    @Mock
    private lateinit var remote: Remote

    @Before
    fun setUp() {
        remoteDataStore = RemoteDataStore(remote)
    }

    @Test
    fun getResultMovies() {
        //Give
        whenever(remote.getResultMovie("")).thenReturn(Single.just(makeListMovieEntity(20)))

        //When
        val testObserver = remote.getResultMovie("").test()

        //Then
        testObserver.assertComplete()
    }
}