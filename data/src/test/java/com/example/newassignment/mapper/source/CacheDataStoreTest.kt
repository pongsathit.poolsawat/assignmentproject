package com.example.newassignment.mapper.source

import com.example.newassignment.mapper.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.mapper.test.factory.Factory.Factory.makeListMovieEntity
import com.example.newassignment.mapper.test.factory.Factory.Factory.makeMovieEntity
import com.example.newassignment.model.MovieEntity
import com.example.newassignment.repository.Cache
import com.example.newassignment.source.CacheDataStore
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class CacheDataStoreTest {

    private lateinit var cacheDataStore: CacheDataStore

    @Mock
    private lateinit var cache: Cache

    @Before
    fun setUp() {
        cacheDataStore = CacheDataStore(cache)
    }

    @Test
    fun getResultMovies() {
        //Give
        stubGetCachedMovie(Single.just(makeListMovieEntity(10)))

        //When
        val testObserver = cacheDataStore.getResultMovie("").test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun getFavoriteList() {
        //Give
        stubGetCachedFavoriteMovie(Single.just(makeListMovieEntity(10)))

        //When
        val testObserver = cacheDataStore.getFavoriteList().test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun saveFavorite() {
        //Give
        val movieEntity = makeMovieEntity()
        whenever(cache.saveFavoriteMovie(movieEntity)).thenReturn(Completable.complete())

        //When
        val testObserver = cacheDataStore.saveFavoriteMovie(movieEntity).test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun deleteFavorite() {
        //Give
        val id = randomInt()
        whenever(cache.deleteFavoriteMovie(id)).thenReturn(Completable.complete())

        //When
        val testObserver = cacheDataStore.deleteFavoriteMovie(id).test()

        //Then
        testObserver.assertComplete()
    }

    private fun stubGetCachedMovie(single: Single<List<MovieEntity>>) {
        whenever(cache.getResultMovie("")).thenReturn(single)
    }

    private fun stubGetCachedFavoriteMovie(single: Single<List<MovieEntity>>) {
        whenever(cache.getFavoriteMovie()).thenReturn(single)
    }
}