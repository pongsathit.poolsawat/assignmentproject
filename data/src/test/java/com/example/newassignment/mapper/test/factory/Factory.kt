package com.example.newassignment.mapper.test.factory

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.mapper.test.factory.DataFactory.Factory.randomDouble
import com.example.newassignment.mapper.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.mapper.test.factory.DataFactory.Factory.randomUuid
import com.example.newassignment.model.MovieEntity

class Factory {

    companion object Factory {

        fun makeMovieEntity(): MovieEntity {
            return MovieEntity(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }

        fun makeListMovieEntity(count: Int): List<MovieEntity> {
            val list = mutableListOf<MovieEntity>()
            repeat(count) {
                list.add(makeMovieEntity())
            }
            return list
        }

        fun makeMovie(): Movie {
            return Movie(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }
    }
}