package com.example.newassignment.mapper.mapper

import com.example.newassignment.mapper.MovieMapper
import com.example.newassignment.mapper.test.factory.Factory
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class MovieMapperTest {

    private lateinit var mapper: MovieMapper

    @Before
    fun setUp() {
        mapper = MovieMapper()
    }

    @Test
    fun mapFromEntityMapsData() {
        //Give
        val entity = Factory.makeMovieEntity()

        //When
        val domain = mapper.mapFromEntity(entity)

        //Then
        assertEquals(entity.id, domain.id)
        assertEquals(entity.overview, domain.overview)
        assertEquals(entity.poster_path, domain.poster_path)
        assertEquals(entity.release_date, domain.release_date)
        assertEquals(entity.title, domain.title)
        assertEquals(entity.vote_average, domain.vote_average)
    }

    @Test
    fun mapToEntityMapsData() {
        //Give
        val domain = Factory.makeMovie()

        //When
        val entity = mapper.mapToEntity(domain)

        //Then
        assertEquals(entity.id, domain.id)
        assertEquals(entity.overview, domain.overview)
        assertEquals(entity.poster_path, domain.poster_path)
        assertEquals(entity.release_date, domain.release_date)
        assertEquals(entity.title, domain.title)
        assertEquals(entity.vote_average, domain.vote_average)
    }
}