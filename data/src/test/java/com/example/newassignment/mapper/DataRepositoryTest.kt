package com.example.newassignment.mapper

import com.example.newassignment.DataRepository
import com.example.newassignment.mapper.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.mapper.test.factory.Factory.Factory.makeListMovieEntity
import com.example.newassignment.mapper.test.factory.Factory.Factory.makeMovie
import com.example.newassignment.mapper.test.factory.Factory.Factory.makeMovieEntity
import com.example.newassignment.source.CacheDataStore
import com.example.newassignment.source.DataStoreFactory
import com.example.newassignment.source.RemoteDataStore
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DataRepositoryTest {

    private lateinit var dataRepository: DataRepository

    @Mock
    private lateinit var dataStoreFactory: DataStoreFactory

    @Mock
    private lateinit var cacheDataStore: CacheDataStore

    @Mock
    private lateinit var remoteDataStore: RemoteDataStore

    @Mock
    private lateinit var movieMapper: MovieMapper

    @Before
    fun setUp() {
        dataRepository = DataRepository(
            dataStoreFactory,
            movieMapper
        )
        stubDataStoreFactoryRetrieveRemoteDataStore()
        stubDataStoreFactoryRetrieveCachedDataStore()
    }

    private fun stubDataStoreFactoryRetrieveRemoteDataStore() {
        whenever(dataStoreFactory.retrieveRemoteDataStore()).thenReturn(remoteDataStore)
    }

    private fun stubDataStoreFactoryRetrieveCachedDataStore() {
        whenever(dataStoreFactory.retrieveCacheDataStore()).thenReturn(cacheDataStore)
    }

    @Test
    fun getResultMovieComplete() {
        //Give
        val movies = makeListMovieEntity(20)
        whenever(remoteDataStore.getResultMovie("")).thenReturn(Single.just(movies))

        //When
        val testObserver = dataRepository.getResultMovie("").test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun saveFavoriteMovie() {
        //Give
        val movie = makeMovie()
        val movieEntity = makeMovieEntity()
        whenever(cacheDataStore.saveFavoriteMovie(any())).thenReturn(Completable.complete())
        whenever(movieMapper.mapToEntity(movie)).thenReturn(movieEntity)

        //When
        val testObserver = dataRepository.saveFavoriteMovie(movie).test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun deleteFavoriteMovie() {
        //Give
        val id = randomInt()
        whenever(cacheDataStore.deleteFavoriteMovie(any())).thenReturn(Completable.complete())

        //When
        val testObserver = dataRepository.deleteFavoriteMovie(id).test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun getFavoriteList() {
        //Give
        val movies = makeListMovieEntity(20)
        whenever(cacheDataStore.getFavoriteList()).thenReturn(Single.just(movies))

        //When
        val testObserver = dataRepository.getFavoriteList().test()

        //Then
        testObserver.assertComplete()
    }

}