package com.example.newassignment.source

import com.example.newassignment.model.MovieEntity
import com.example.newassignment.repository.Cache
import com.example.newassignment.repository.DataStore
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

open class CacheDataStore @Inject constructor(private val cache: Cache) : DataStore {
    override fun getResultMovie(query: String): Single<List<MovieEntity>> {
        return cache.getResultMovie(query)
    }

    override fun getFavoriteList(): Single<List<MovieEntity>> {
        return cache.getFavoriteMovie()
    }

    override fun saveFavoriteMovie(movie: MovieEntity): Completable {
        return cache.saveFavoriteMovie(movie)
    }

    override fun deleteFavoriteMovie(id: Int): Completable {
        return cache.deleteFavoriteMovie(id)
    }

}