package com.example.newassignment.source

import com.example.newassignment.model.MovieEntity
import com.example.newassignment.repository.DataStore
import com.example.newassignment.repository.Remote
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.UnsupportedOperationException
import javax.inject.Inject

open class RemoteDataStore @Inject constructor(
    private val remote: Remote
) : DataStore {
    override fun getResultMovie(query: String): Single<List<MovieEntity>> {
        return remote.getResultMovie(query)
    }

    override fun getFavoriteList(): Single<List<MovieEntity>> {
        throw UnsupportedOperationException()
    }

    override fun saveFavoriteMovie(movie: MovieEntity): Completable {
        throw UnsupportedOperationException()
    }

    override fun deleteFavoriteMovie(id: Int): Completable {
        throw UnsupportedOperationException()
    }
}