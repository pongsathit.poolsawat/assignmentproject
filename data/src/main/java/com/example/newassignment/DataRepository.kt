package com.example.newassignment

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.repository.Repository
import com.example.newassignment.mapper.MovieMapper
import com.example.newassignment.source.DataStoreFactory
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class DataRepository @Inject constructor(
    private val factory: DataStoreFactory,
    private val movieMapper: MovieMapper
) : Repository {
    override fun getResultMovie(query: String): Single<List<Movie>> {
        val dataStore = factory.retrieveRemoteDataStore()
        return dataStore.getResultMovie(query).map {
            it.map { movieEntity ->
                movieMapper.mapFromEntity(movieEntity)
            }
        }
    }

    override fun getFavoriteList(): Single<List<Movie>> {
        val dataStore = factory.retrieveCacheDataStore()
        return dataStore.getFavoriteList().map {
            it.map { movieMapper.mapFromEntity(it) }
        }
    }

    override fun saveFavoriteMovie(movie: Movie): Completable {
        val dataStore = factory.retrieveCacheDataStore()
        return dataStore.saveFavoriteMovie(movieMapper.mapToEntity(movie))
    }

    override fun deleteFavoriteMovie(id: Int): Completable {
        val dataStore = factory.retrieveCacheDataStore()
        return dataStore.deleteFavoriteMovie(id)
    }


}