package com.example.newassignment.mapper

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.model.MovieEntity
import javax.inject.Inject

open class MovieMapper @Inject constructor() :
    Mapper<MovieEntity, Movie> {
    override fun mapFromEntity(type: MovieEntity): Movie {
        return Movie(
            type.title,
            type.overview,
            type.release_date,
            type.poster_path,
            type.vote_average,
            type.id
        )
    }

    override fun mapToEntity(type: Movie): MovieEntity {
        return MovieEntity(
            type.title,
            type.overview,
            type.release_date,
            type.poster_path,
            type.vote_average,
            type.id
        )
    }


}