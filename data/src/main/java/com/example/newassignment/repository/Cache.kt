package com.example.newassignment.repository

import com.example.newassignment.model.MovieEntity
import io.reactivex.Completable
import io.reactivex.Single

interface Cache {

    fun getResultMovie(query: String): Single<List<MovieEntity>>

    fun getFavoriteMovie(): Single<List<MovieEntity>>

    fun saveFavoriteMovie(movieEntity: MovieEntity): Completable

    fun deleteFavoriteMovie(id: Int): Completable

}