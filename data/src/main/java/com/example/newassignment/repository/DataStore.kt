package com.example.newassignment.repository

import com.example.newassignment.model.MovieEntity
import io.reactivex.Completable
import io.reactivex.Single

interface DataStore {

    fun getResultMovie(query: String): Single<List<MovieEntity>>

    fun getFavoriteList(): Single<List<MovieEntity>>

    fun saveFavoriteMovie(movie: MovieEntity): Completable

    fun deleteFavoriteMovie(id: Int): Completable

}