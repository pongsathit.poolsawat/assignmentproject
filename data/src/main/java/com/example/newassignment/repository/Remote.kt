package com.example.newassignment.repository

import com.example.newassignment.model.MovieEntity
import io.reactivex.Single

interface Remote {

    fun getResultMovie(query: String): Single<List<MovieEntity>>

}