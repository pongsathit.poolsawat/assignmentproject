package com.example.newassignment.domain.interactor.saveFavoriteMovie

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.repository.Repository
import io.reactivex.Single
import javax.inject.Inject

open class SaveFavoriteMovie @Inject constructor(
    val repository: Repository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleUseCase<Boolean, Movie>(threadExecutor, postExecutionThread) {
    public override fun buildUseCaseObservable(params: Movie?): Single<Boolean> {
        params?.let {
            repository.saveFavoriteMovie(params).doOnComplete {
                Single.just(true)
            }.subscribe()
        }
        return Single.just(false)
    }

}