package com.example.newassignment.domain.repository

import com.example.newassignment.domain.model.Movie
import io.reactivex.Completable
import io.reactivex.Single

interface Repository {

    fun getResultMovie(query: String): Single<List<Movie>>

    fun getFavoriteList(): Single<List<Movie>>

    fun saveFavoriteMovie(movie: Movie): Completable

    fun deleteFavoriteMovie(id: Int): Completable

}