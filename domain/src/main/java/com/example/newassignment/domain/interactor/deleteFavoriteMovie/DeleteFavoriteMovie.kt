package com.example.newassignment.domain.interactor.deleteFavoriteMovie

import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.repository.Repository
import io.reactivex.Single
import javax.inject.Inject

open class DeleteFavoriteMovie @Inject constructor(
    val repository: Repository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleUseCase<Boolean, Int>(threadExecutor, postExecutionThread) {
    public override fun buildUseCaseObservable(params: Int?): Single<Boolean> {
        params?.let {
            repository.deleteFavoriteMovie(params).doOnComplete {
                Single.just(true)
            }.subscribe()
        }
        return Single.just(false)
    }

}