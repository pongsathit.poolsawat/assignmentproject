package com.example.newassignment.domain.interactor.getFavoriteList

import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.SingleUseCase
import com.example.newassignment.domain.repository.Repository
import io.reactivex.Single
import javax.inject.Inject

open class GetFavoriteList @Inject constructor(
    val repository: Repository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleUseCase<List<Movie>, Void?>(threadExecutor, postExecutionThread) {
    public override fun buildUseCaseObservable(params: Void?): Single<List<Movie>> {
        return repository.getFavoriteList()
    }

}