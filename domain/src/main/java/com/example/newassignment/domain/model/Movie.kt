package com.example.newassignment.domain.model

data class Movie (
    val title: String?,
    val overview: String?,
    val release_date: String?,
    val poster_path: String?,
    val vote_average: Double?,
    val id: Int?
)
