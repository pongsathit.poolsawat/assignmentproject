package com.example.newassignment.domain.interactor.deleteFavoriteMovie.usecase

import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.Factory.Factory.makeListMovie
import com.example.newassignment.domain.interactor.getResultMovie.GetResultMovie
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.repository.Repository
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetResultMovieTest {

    private lateinit var useCase: GetResultMovie

    @Mock
    private lateinit var mockThreadExecutor: ThreadExecutor

    @Mock
    private lateinit var mockPostExecutionThread: PostExecutionThread

    @Mock
    private lateinit var mockRepository: Repository

    @Before
    fun setUp() {
        useCase = GetResultMovie(
            mockRepository,
            mockThreadExecutor,
            mockPostExecutionThread
        )
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {
//        //When
//        useCase.buildUseCaseObservable()
//        //Then
//        verify(mockRepository).getFavoriteList()
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        //Give
        val movies = makeListMovie(10)
        stubRepositoryGetResultMovies(Single.just(movies))

        //When
        val testObserver = useCase.buildUseCaseObservable().test()

        //Then
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseObservableReturnData() {
        //Give
        val movies = makeListMovie(10)
        stubRepositoryGetResultMovies(Single.just(movies))

        //When
        val testObserver = useCase.buildUseCaseObservable().test()

        //Then
        testObserver.assertValue(movies)
    }

    private fun stubRepositoryGetResultMovies(single: Single<List<Movie>>) {
        whenever(mockRepository.getResultMovie("")).thenReturn(single)
    }
}