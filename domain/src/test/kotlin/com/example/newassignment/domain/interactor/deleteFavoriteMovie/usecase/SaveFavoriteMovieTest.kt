package com.example.newassignment.domain.interactor.deleteFavoriteMovie.usecase

import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.Factory.Factory.makeMovie
import com.example.newassignment.domain.interactor.saveFavoriteMovie.SaveFavoriteMovie
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.repository.Repository
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SaveFavoriteMovieTest {

    private lateinit var useCase: SaveFavoriteMovie

    @Mock
    private lateinit var mockThreadExecutor: ThreadExecutor

    @Mock
    private lateinit var mockPostExecutionThread: PostExecutionThread

    @Mock
    private lateinit var mockRepository: Repository

    @Before
    fun setUp() {
        useCase = SaveFavoriteMovie(
            mockRepository,
            mockThreadExecutor,
            mockPostExecutionThread
        )
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {
        //Give
        val movies = makeMovie()
        stubRepositorySaveFavoriteMovie(movies, Completable.complete())
        //When
        useCase.buildUseCaseObservable(movies)
        //Then
        verify(mockRepository).saveFavoriteMovie(movies)
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        //Give
        val movie = makeMovie()
        stubRepositorySaveFavoriteMovie(movie, Completable.complete())

        //When
        val testObserver = useCase.buildUseCaseObservable(movie).test()

        //Then
        testObserver.assertComplete()
    }

    private fun stubRepositorySaveFavoriteMovie(movie: Movie, completable: Completable) {
        whenever(mockRepository.saveFavoriteMovie(movie)).thenReturn(completable)
    }
}