package com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory

import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.DataFactory.Factory.randomDouble
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.DataFactory.Factory.randomUuid
import com.example.newassignment.domain.model.Movie

class Factory {

    companion object Factory {

        fun makeMovie(): Movie {
            return Movie(
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomInt()
            )
        }

        fun makeListMovie(count: Int): List<Movie> {
            val list = mutableListOf<Movie>()
            repeat(count) {
                list.add(makeMovie())
            }
            return list
        }
    }
}