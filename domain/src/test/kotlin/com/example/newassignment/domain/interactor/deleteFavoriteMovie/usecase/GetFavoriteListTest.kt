package com.example.newassignment.domain.interactor.deleteFavoriteMovie.usecase

import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.Factory.Factory.makeListMovie
import com.example.newassignment.domain.interactor.getFavoriteList.GetFavoriteList
import com.example.newassignment.domain.model.Movie
import com.example.newassignment.domain.repository.Repository
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetFavoriteListTest {

    private lateinit var useCase: GetFavoriteList

    @Mock
    private lateinit var mockThreadExecutor: ThreadExecutor

    @Mock
    private lateinit var mockPostExecutionThread: PostExecutionThread

    @Mock
    private lateinit var mockRepository: Repository

    @Before
    fun setUp() {
        useCase = GetFavoriteList(
            mockRepository,
            mockThreadExecutor,
            mockPostExecutionThread
        )
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {
        //When
        useCase.buildUseCaseObservable()
        //Then
        verify(mockRepository).getFavoriteList()
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        //Give
        val movies = makeListMovie(20)
        stubRepositoryGetFavoriteList(Single.just(movies))

        //When
        val testObserver = useCase.buildUseCaseObservable().test()

        //Then
        testObserver.assertValue(movies)
    }

    private fun stubRepositoryGetFavoriteList(single: Single<List<Movie>>) {
        whenever(mockRepository.getFavoriteList()).thenReturn(single)
    }
}