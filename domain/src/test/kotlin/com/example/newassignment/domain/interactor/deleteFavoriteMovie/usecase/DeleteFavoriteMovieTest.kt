package com.example.newassignment.domain.interactor.deleteFavoriteMovie.usecase

import com.example.newassignment.domain.executor.PostExecutionThread
import com.example.newassignment.domain.executor.ThreadExecutor
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.DeleteFavoriteMovie
import com.example.newassignment.domain.interactor.deleteFavoriteMovie.test.factory.DataFactory.Factory.randomInt
import com.example.newassignment.domain.repository.Repository
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class DeleteFavoriteMovieTest {

    private lateinit var useCase: DeleteFavoriteMovie

    @Mock
    private lateinit var mockThreadExecutor: ThreadExecutor

    @Mock
    private lateinit var mockPostExecutionThread: PostExecutionThread

    @Mock
    private lateinit var mockRepository: Repository

    @Before
    fun setUp() {
        useCase = DeleteFavoriteMovie(
            mockRepository,
            mockThreadExecutor,
            mockPostExecutionThread
        )
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {

        //Give
        val id = randomInt()
        stubRepositoryDeleteMovie(id, Completable.complete())

        //When
        val testObserver = useCase.buildUseCaseObservable(id).test()

        //Then
        testObserver.assertComplete()
    }

    private fun stubRepositoryDeleteMovie(id: Int, completable: Completable) {
        whenever(mockRepository.deleteFavoriteMovie(id)).thenReturn(completable)
    }
}